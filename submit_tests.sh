#!/bin/bash
# Copyright (C) 2020, Renesas Electronics Europe GmbH, Chris Paterson
# <chris.paterson2@renesas.com>
#
# This script scans the OUTPUT_DIR for any built Kernels and creates/submits the
# relevant test jobs to the CIP LAVA master.
#
# Script specific dependencies:
# lavacli aws pwd sed date
#
# The following must be set in GitLab CI variables for lavacli to work:
# $CIP_LAVA_LAB_USER
# $CIP_LAVA_LAB_TOKEN
#
# The following must be set in GitLab CI variables for aws to work:
# $CIP_CI_AWS_ID
# $CIP_CI_AWS_KEY
# $CIP_LAVA_LAB_SERVER (default: lava.ciplatform.org)
# $CIP_AWS_URL_UP (default: s3://download.cip-project.org/ciptesting/ci)
# $CIP_AWS_URL_DOWN (default: https://s3-us-west-2.amazonaws.com/download.cip-project.org/ciptesting/ci)
#
# Global environment variables used by this script:
# TEST_TIMEOUT: Length of time in minutes to wait for test completion. If unset
#               then no timeout is used.
# SUBMIT_ONLY:  Set to 'true' if you don't want to wait to see if submitted LAVA
#               jobs complete. If this is not set a default of 'false' is used.
# BUILD_ARCH:   The architecture used in the build job. Required to help with
#               directory navigation.
# CONFIG:       The name of the configuration file used in the build job.
#               Required to help with directory navigation.
# DEVICES:      A list of device-types as defined in LAVA that are to be tested.
# DTBS:         A list of device tree blobs (including extension) that are to be
#               used in testing. There are three ways to use/not use this
#               parameter:
#               * Not define it at all. This is okay as not all devices use
#                 device trees.
#               * If multiple devices are defined, define one dtb per device.
#                 They will be mapped on a one-to-one basis.
#               * If only one device is defined, one or many dtbs may be
#                 defined. They will all be used on the specified device.
# TESTS:        A list of test cases to run for each device/dtb combo. Currently
#               supported tests are:
#               * boot: A simple boot test that prints the Kernel version.
#               * smc: A Spectre/Meltdown vulnerability checker.
#               * ltp: Runs through all supported tests from the Linux Test
#                 Project test suit including ltp-cve-tests, ltp-dio-tests,
#                 ltp-fs-tests, ltp-ipc-tests, ltp-math-tests, ltp-sched-tests,
#                 ltp-syscalls-tests and ltp-timers-tests.
#               * ltp-[TEST]: Specify an individual test from the list of LTP
#                 tests above.
#               * cyclictest: Measures event latency in the Linux Kernel.
#               * cyclicdeadline: Similar to cyclictest but uses SCHED_DEADLINE
#                 to measure jitter.
#               * hackbench: Linux scheduler stress test.
#               * cyclictest+hackbench: Runs cyclictest with hackbench running
#                 in the background.
# CI_COMMIT_REF_NAME: Used as part of the LAVA job description. Automatically
#                     set by GitLab CI.
#
################################################################################

set -e

################################################################################
WORK_DIR=$(pwd)
OUTPUT_DIR="$WORK_DIR/output"
TEMPLATE_DIR="/opt/lava_templates"
################################################################################
AWS_URL_UP="${CIP_AWS_URL_UP:-s3://download.cip-project.org/ciptesting/ci}"
AWS_URL_DOWN="${CIP_AWS_URL_DOWN:-https://s3-us-west-2.amazonaws.com/download.cip-project.org/ciptesting/ci}"
LAVACLI_ARGS="--uri https://$CIP_LAVA_LAB_USER:$CIP_LAVA_LAB_TOKEN@${CIP_LAVA_LAB_SERVER:-lava.ciplatform.org}/RPC2"
LAVA_JOBS_URL="https://${CIP_LAVA_LAB_SERVER:-lava.ciplatform.org}/scheduler/job"
INDEX="0"
if [ -z "$SUBMIT_ONLY" ]; then SUBMIT_ONLY=false; fi
################################################################################

set_up () {
	TMP_DIR="$(mktemp -d)"
}

clean_up () {
	rm -rf "$TMP_DIR"
}

# $1: OS type
# $2: Test name
create_job_definition () {
	local base_os="$1"
	local testname="$2"

	local dtb_url="$AWS_URL_DOWN/$DTB"
	local kernel_url="$AWS_URL_DOWN/$KERNEL"
	local modules_url="$AWS_URL_DOWN/$MODULES"

	local commit_ref=$(echo "${CI_COMMIT_REF_NAME}" | sed 's/\//-/g')

	if $USE_DTB; then
		local job_name="${commit_ref}_${VERSION}_${BUILD_ARCH}_${CONFIG}_${DTB_NAME}_${testname}"
	else
		local job_name="${commit_ref}_${VERSION}_${BUILD_ARCH}_${CONFIG}_${testname}"
	fi

	local job_definition="$TMP_DIR/${INDEX}_${job_name}.yaml"
	INDEX=$((INDEX+1))

	cat ${TEMPLATE_DIR}/header_base.yaml > "${job_definition}"
	if [ "${DEVICE}" = "qemu" ] ; then
		local device_name="${DEVICE}_${BUILD_ARCH}"
		cat ${TEMPLATE_DIR}/context_${device_name}.yaml >> "${job_definition}"
	else
		local device_name="${DEVICE}"
	fi

	cat ${TEMPLATE_DIR}/timeouts_"${testname}".yaml \
	    ${TEMPLATE_DIR}/action_"${base_os}"_"${device_name}".yaml \
	    ${TEMPLATE_DIR}/boot_"${base_os}"_"${device_name}".yaml \
	    ${TEMPLATE_DIR}/test_"${testname}".yaml \
	    >> "${job_definition}"

	sed -i "s|DEVICE_NAME|${DEVICE}|g" "$job_definition"
	sed -i "s|JOB_NAME|$job_name|g" "$job_definition"
	if $USE_MODULES; then
		sed -i "/DTB_URL/ a \    modules:\n      url: $modules_url\n      compression: gz" "$job_definition"
	fi
	if $USE_DTB; then
		sed -i "s|DTB_URL|$dtb_url|g" "$job_definition"
	fi
	sed -i "s|KERNEL_URL|$kernel_url|g" "$job_definition"
}

configure_aws () {
	aws configure set aws_access_key_id "$CIP_CI_AWS_ID"
	aws configure set aws_secret_access_key "$CIP_CI_AWS_KEY"
	aws configure set default.region us-west-2
	aws configure set default.output text
}

upload_binaries () {
	configure_aws

	# Note: If there are multiple jobs in the same pipeline building the
	# same SHA, same BUILD_ARCH and same CONFIG name, AWS binaries will be
	# overwritten.
	echo "------------------"
	echo "Uploading binaries"
	echo "------------------"
	aws s3 sync "$OUTPUT_DIR"/. $AWS_URL_UP --exclude jobs --acl public-read
}

print_job_info () {
	echo "-----------------"
	echo "Creating test job"
	echo "-----------------"
	echo "Version: $VERSION"
	echo "Arch: $BUILD_ARCH"
	echo "Config: $CONFIG"
	echo "Device: $DEVICE"
	echo "Kernel: $KERNEL_NAME"
	echo "DTB: $DTB_NAME"
	echo "Modules: $MODULES_NAME"
	echo "Test: $TEST"
}

# $1: Device to use
# $2: Test to run
# $3: DTB to use (optional)
create_job() {
	VERSION=$(ls "${OUTPUT_DIR}")
	local bin_dir=${VERSION}/${BUILD_ARCH}/${CONFIG}
	KERNEL_NAME=$(ls "${OUTPUT_DIR}"/"${bin_dir}"/kernel)
	KERNEL=${bin_dir}/kernel/${KERNEL_NAME}
	DEVICE=${1}
	TEST=${2}

	if [ -n "${3}" ]; then
		DTB_NAME=${3}
		DTB=${bin_dir}/dtb/${DTB_NAME}
		USE_DTB=true
	else
		DTB_NAME="N/A"
		USE_DTB=false
	fi

	if [ -d "${bin_dir}/modules" ]; then
		MODULES_NAME=$(ls "${OUTPUT_DIR}"/"${bin_dir}"/modules)
		MODULES=${bin_dir}/modules/${MODULES_NAME}
		USE_MODULES=true
	else
		MODULES_NAME="N/A"
		USE_MODULES=false
	fi

	print_job_info

	case ${TEST} in
		"boot")
			create_job_definition oe boot
			;;

		"smc")
			create_job_definition oe smc
			;;

		"ltp")
			create_job_definition deby-buster-ltp ltp-cve-tests
			create_job_definition deby-buster-ltp ltp-dio-tests
			create_job_definition deby-buster-ltp ltp-fs-tests
			create_job_definition deby-buster-ltp ltp-ipc-tests
			create_job_definition deby-buster-ltp ltp-math-tests
			create_job_definition deby-buster-ltp ltp-sched-tests
			create_job_definition deby-buster-ltp ltp-syscalls-tests
			create_job_definition deby-buster-ltp ltp-timers-tests
			;;

		"ltp-cve-tests" | \
		"ltp-dio-tests" | \
		"ltp-fs-tests" | \
		"ltp-ipc-tests" | \
		"ltp-math-tests" | \
		"ltp-sched-tests" | \
		"ltp-syscalls-tests" | \
		"ltp-timers-tests" )
			create_job_definition deby-buster-ltp "${test[$i]}"
			;;

		"cyclictest")
			create_job_definition oe cyclictest
			;;

		"cyclicdeadline")
			create_job_definition oe cyclicdeadline
			;;

		"hackbench")
			create_job_definition oe hackbench
			;;

		"cyclictest+hackbench")
			create_job_definition oe cyclictest+hackbench
			;;
	esac
}

create_jobs() {
	if [ -z "$DEVICES" ]; then
		echo "No devices defined, so cannot test."
		return 1
	fi

	# Convert $DEVICES into an array
	local devices=($DEVICES)

	# Convert $DTBS into an array
	local dtbs=($DTBS)

	# Convert $TESTS into an array
	local tests=($TESTS)

	# Use boot test as default if none are defined
	if [ -z "$tests" ]; then
		tests="boot"
	fi

	# Device tree
	if [ -z "$dtbs" ]; then
		# Add job for each device/test combo
		for i in "${!devices[@]}"; do
			for j in "${!tests[@]}"; do
				create_job \
					"${devices[$i]}" \
					${tests[$j]}
			done
		done
	else
		# If there is only one device defined, assume that all device
		# trees are for that device
		if [ "${#devices[@]}" -eq 1 ]; then
			# Add job for each dtb/test combo
			for i in "${!dtbs[@]}"; do
				for j in "${!tests[@]}"; do
					create_job \
						"${devices[0]}" \
						${tests[$j]} \
						"${dtbs[$i]}"
				done
			done
		else
			# Check there is a dtb for each defined device
			if [ "${#devices[@]}" -ne "${#dtbs[@]}" ]; then
				echo "Number of devices does not equal the number of dtbs."
				echo "If more than one device is specified, each must have a dtbs defined."
				clean_up
				return 1
			fi

			# Add job for each device/dtb/test combo
			for i in "${!dtbs[@]}"; do
				for j in "${!tests[@]}"; do
					create_job \
						"${devices[$i]}" \
						${tests[$j]} \
						"${dtbs[$i]}"
				done
			done
		fi
	fi

	return 0
}

# $1: Job description yaml file
submit_job() {
	# TODO: Add yaml validation
        # Make sure yaml file exists
	if [ -f "$1" ]; then
		echo "Submitting $1 to LAVA master..."
		# Catch error that occurs if invalid yaml file is submitted
		local ret=$(lavacli $LAVACLI_ARGS jobs submit "$1") || error=true

		if [[ $ret != [0-9]* ]]
		then
			echo "Something went wrong with job submission. LAVA returned:"
			echo "${ret}"
		else
			echo "Job submitted successfully as #${ret}."

			local lavacli_output=$TMP_DIR/lavacli_output
			lavacli $LAVACLI_ARGS jobs show "${ret}" \
				> "$lavacli_output"

			local status=$(cat "$lavacli_output" \
				| grep "state" \
				| cut -d ":" -f 2 \
				| awk '{$1=$1};1')
			STATUS[${ret}]=$status

			local health=$(cat "$lavacli_output" \
				| grep "Health" \
				| cut -d ":" -f 2 \
				| awk '{$1=$1};1')
			HEALTH[${ret}]=$health

			local device_type=$(cat "$lavacli_output" \
				| grep "device-type" \
				| cut -d ":" -f 2 \
				| awk '{$1=$1};1')
			DEVICE_TYPE[${ret}]=$device_type

			local device=$(cat "$lavacli_output" \
				| grep "device      :" \
				| cut -d ":" -f 2 \
				| awk '{$1=$1};1')
			DEVICE[${ret}]=$device

			local test=$(cat "$lavacli_output" \
				| grep "description" \
				| rev | cut -d "_" -f 1 | rev)
			TEST[${ret}]=$test

			JOBS+=("${ret}")
		fi
	fi
}

# $1: Device-type to search for
is_device_online () {
	local lavacli_output=$TMP_DIR/lavacli_output

	# Get list of all devices
	lavacli $LAVACLI_ARGS devices list > "$lavacli_output"

	# Count the number of online devices
	local count=$(grep "(${1})" "$lavacli_output" | grep -c "Good")
	echo "There are currently $count \"${1}\" devices online."

	if [ "$count" -gt 0 ]; then
		return 0
	fi
	return 1
}

submit_jobs () {
	local error=false

	for JOB in "$TMP_DIR"/*.yaml; do
		local device=$(grep device_type "$JOB" | cut -d ":" -f 2 | awk '{$1=$1};1')
		if is_device_online "$device"; then
			submit_job "$JOB"
		else
			echo "Refusing to submit test job as there are no suitable devices available."
			error=true
		fi
	done

	if $error; then
		return 1
	fi
	return 0
}

check_if_all_finished () {
        for i in "${JOBS[@]}"
        do
                if [ "${STATUS[$i]}" != "Finished" ]; then
                        return 1
                fi
        done
        return 0
}

check_for_test_error () {
        for i in "${JOBS[@]}"
        do
		if [ "${HEALTH[$i]}" != "Complete" ]; then
			return 0
		fi
	done
	return 1
}

# $1: LAVA job ID to show results for
get_test_result () {
	if [ -n "${1}" ]; then
		lavacli $LAVACLI_ARGS results "${1}"
	fi
}

get_test_results () {
	for i in "${JOBS[@]}"
	do
		get_test_result "${i}"
	done
}

# $1: Test to print before job summaries
# $2: Set to true to print results for each job
print_status () {
	if [ -z "${1}" ]; then
	# Set default text
		local message="Current job status:"
	else
		local message="${1}"
	fi

	echo "------------------------------"
	echo "${message}"
	echo "------------------------------"
	for i in "${JOBS[@]}"; do
		echo "Job #$i: ${STATUS[$i]}"
		echo "Health: ${HEALTH[$i]}"
		echo "Device Type: ${DEVICE_TYPE[$i]}"
		echo "Device: ${DEVICE[$i]}"
		echo "Test: ${TEST[$i]}"
		echo "URL: ${LAVA_JOBS_URL}/$i"
		if [ -n "${2}" ]; then
			get_test_result "$i"
		fi
		echo " "
	done
}

print_current_status () {
	print_status "Current job status:"
}

print_summary () {
	echo "------------------------------"
	echo "Job Summary"
	echo "------------------------------"
	for i in "${JOBS[@]}"
	do
		echo "Job #${i} ${STATUS[$i]}. Job health: ${HEALTH[$i]}. URL: ${LAVA_JOBS_URL}/${i}"
	done
}

check_status () {
	if [ -n "$TEST_TIMEOUT" ]; then
		# Current time + timeout time
		local end_time=$(date +%s -d "+ $TEST_TIMEOUT min")
	fi

	local error=false

	if [ ${#JOBS[@]} -ne 0 ]
	then
		print_current_status

		while true
		do
			# Get latest status
			for i in "${JOBS[@]}"
			do
				if [ "${STATUS[$i]}" != "Finished" ]
				then
					local lavacli_output=$TMP_DIR/lavacli_output
					lavacli $LAVACLI_ARGS jobs show "$i" \
						> "$lavacli_output"

					local status=$(cat "$lavacli_output" \
						| grep "state" \
						| cut -d ":" -f 2 \
						| awk '{$1=$1};1')

					local health=$(cat "$lavacli_output" \
						| grep "Health" \
						| cut -d ":" -f 2 \
						| awk '{$1=$1};1')
					HEALTH[$i]=$health

					local device_type=$(cat "$lavacli_output" \
						| grep "device-type" \
						| cut -d ":" -f 2 \
						| awk '{$1=$1};1')
					DEVICE_TYPE[$i]=$device_type

					local device=$(cat "$lavacli_output" \
						| grep "device      :" \
						| cut -d ":" -f 2 \
						| awk '{$1=$1};1')
					DEVICE[$i]=$device

					if [ "${STATUS[$i]}" != "$status" ]; then
						STATUS[$i]=$status

						# Something has changed
						print_current_status
					else
						STATUS[$i]=$status
					fi
				fi
			done

			if check_if_all_finished; then
				break
			fi

			if [ -n "$TEST_TIMEOUT" ]; then
				# Check timeout
				local now=$(date +%s)
				if [ "$now" -ge "$end_time" ]; then
					echo "Timed out waiting for test jobs to complete"
					error=true
					break
				fi
			fi

			# Small wait to avoid spamming the server too hard
			sleep 10
		done

		if check_if_all_finished; then
			# Print job outcome
			print_status "Final job status:" true

			if check_for_test_error; then
				error=true
			fi
		fi
	fi

	if $error; then
		echo "---------------------"
		echo "Errors during testing"
		echo "---------------------"
		print_summary
		clean_up
		return 1
	fi

	echo "-----------------------------------"
	echo "All submitted tests were successful"
	echo "-----------------------------------"
	print_summary
	return 0
}

ERROR=false

trap clean_up SIGHUP SIGINT SIGTERM
set_up

if ! create_jobs; then
	clean_up
	exit 1
fi

upload_binaries

if ! submit_jobs; then
	ERROR=true
fi

if ! $SUBMIT_ONLY; then
	if ! check_status; then
		ERROR=true
	fi
fi

clean_up

if $ERROR; then
	exit 1
fi

exit 0
